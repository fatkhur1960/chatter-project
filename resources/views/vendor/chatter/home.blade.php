@extends(Config::get('chatter.master_file_extend'))

@section(Config::get('chatter.yields.head'))
    <link href="/vendor/devdojo/chatter/assets/vendor/spectrum/spectrum.css" rel="stylesheet">
	<link href="/vendor/devdojo/chatter/assets/css/chatter.css" rel="stylesheet">
	@if($chatter_editor == 'simplemde')
		<link href="/vendor/devdojo/chatter/assets/css/simplemde.min.css" rel="stylesheet">
	@endif
@stop

@section('content')

<div id="app-padding-container" class="chatter_home">

	<h1 class="uk-margin-remove-bottom dd-title">{{ Config::get('app.name') }}</h1>
	<h2 class="uk-margin-remove-top uk-margin-medium-bottom dd-subtitle">{{ Config::get('chatter.description') }}</h2>

	@if(Session::has('chatter_alert'))
		<div class="container">
			<div class="chatter-alert alert alert-{{ Session::get('chatter_alert_type') }}">
	        	<strong><i class="chatter-alert-{{ Session::get('chatter_alert_type') }}"></i> {{ Config::get('chatter.alert_messages.' . Session::get('chatter_alert_type')) }}</strong>
	        	{{ Session::get('chatter_alert') }}
	        	<i class="chatter-close"></i>
	        </div>
	    </div>
	    <div class="chatter-alert-spacer"></div>
	@endif

	@if (count($errors) > 0)
		<div class="container">
			<div class="chatter-alert alert alert-danger">
	    		<p><strong><i class="chatter-alert-danger"></i> {{ Config::get('chatter.alert_messages.danger') }}</strong> Please fix the following errors:</p>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
				</ul>
		    </div>
	    </div>
	@endif

	<div class="container chatter_container">

		<div class="uk-width-4-5@l uk-width-2-3@m right-column">
			<div class="panel">
				@foreach($discussions as $discussion)
				@endforeach
				@if( Request::is( Config::get('chatter.routes.home')) )
					<div class="label-viewing"> Viewing All Discussions</div>
				@elseif( Request::is( Config::get('chatter.routes.home') . '/' . Config::get('chatter.routes.category') . '/*' ) && isset( $discussion ) )
					<div class="label-viewing">
						<div class="catdot" style="background: {{ $discussion->category->color }}"></div>
						Viewing {{ $discussion->category->name }} Discussions
					</div>
				@endif
				<ul class="discussions">
					@foreach($discussions as $discussion)
						<li>
							<a class="discussion_list" href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.discussion') }}/{{ $discussion->category->slug }}/{{ $discussion->slug }}">
								<div class="chatter_avatar">
									@if(Config::get('chatter.user.avatar_image_database_field'))
										
										<?php $db_field = Config::get('chatter.user.avatar_image_database_field'); ?>
										
										<!-- If the user db field contains http:// or https:// we don't need to use the relative path to the image assets -->
										@if( (substr($discussion->user->{$db_field}, 0, 7) == 'http://') || (substr($discussion->user->{$db_field}, 0, 8) == 'https://') )
											<img src="{{ $discussion->user->{$db_field}  }}">
										@else
											<img src="{{ Config::get('chatter.user.relative_url_to_image_assets') . $discussion->user->{$db_field}  }}">
										@endif
									
									@else
										
										<span class="chatter_avatar_circle" style="background-color:#<?= \DevDojo\Chatter\Helpers\ChatterHelper::stringToColorCode($discussion->user->email) ?>">
											{{ strtoupper(substr($discussion->user->email, 0, 1)) }}
										</span>
										
									@endif
								</div>

								<div class="chatter_middle">
									<h3 class="chatter_middle_title">{{ $discussion->title }} <div class="chatter_cat" style="background-color:{{ $discussion->category->color }}">{{ $discussion->category->name }}</div></h3>
									<span class="chatter_middle_details">Posted By: <span data-href="/user">{{ ucfirst($discussion->user->{Config::get('chatter.user.database_field_with_user_name')}) }}</span> {{ \Carbon\Carbon::createFromTimeStamp(strtotime($discussion->created_at))->diffForHumans() }}</span>
									@if($discussion->post[0]->markdown)
										<?php $discussion_body = GrahamCampbell\Markdown\Facades\Markdown::convertToHtml( $discussion->post[0]->body ); ?>
									@else
										<?php $discussion_body = $discussion->post[0]->body; ?>
									@endif
									<p>{{ substr(strip_tags($discussion_body), 0, 200) }}@if(strlen(strip_tags($discussion_body)) > 200){{ '...' }}@endif</p>
								</div>

								<div class="chatter_right">
									
									<div class="chatter_count"><i class="chatter-bubble"></i> {{ $discussion->postsCount[0]->total }}</div>
								</div>

								<div class="chatter_clear"></div>
							</a>
						</li>
					@endforeach
				</ul>
			</div>

			<div class="uk-align-center">
				<div class="uk-pagination uk-flex-center">
					{{ $discussions->links() }}
				</div>
			</div>

		</div>
		
		<div class="uk-width-1-5@l uk-width-1-3@m left-column">
			<!-- SIDEBAR -->
			<div class="chatter_sidebar">
				@guest
				<button class="uk-button uk-button-primary" id="new_discussion_btn">Ask a Question</button>
				@else
				<button class="uk-button uk-button-primary" id="new_discussion_btn" uk-toggle="target: #new-discussion-modal">Ask a Question</button>
				@endguest
				<a class="active" href="/{{ Config::get('chatter.routes.home') }}"><i class="dojo-zap" style="position:relative; top:2px;"></i>All {{ Config::get('chatter.titles.discussions') }}</a>
				<ul class="uk-list">
					<?php $categories = DevDojo\Chatter\Models\Models::category()->all(); ?>
					@foreach($categories as $category)
						<li><a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.category') }}/{{ $category->slug }}"><div class="chatter-box" style="background-color:{{ $category->color }}"></div> {{ $category->name }}</a></li>
					@endforeach
				</ul>
			</div>
			<!-- END SIDEBAR -->
		</div>
			
	</div>

	<div id="new-discussion-modal" uk-modal>
		<div class="chatter_loader dark" id="new_discussion_loader">
			<div></div>
		</div>
		<div class="uk-modal-dialog uk-modal-body">
			<button class="uk-modal-close-default" type="button" uk-close></button>
			<h2 class="uk-modal-title">New Question/Discussion</h2>
			<p>Fill out the information below to begin your new discussion.</p>

			<form id="chatter_form_editor" action="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.discussion') }}" method="POST">
				<input type="text" class="uk-input uk-margin-small-bottom" id="title" name="title" placeholder="Title of Discussion" v-model="title" >		
				<select id="chatter_category_id" class="uk-select uk-margin-small-bottom" name="chatter_category_id">
					<option value="">Select a Category</option>
					@foreach($categories as $category)
						@if(old('chatter_category_id') == $category->id)
							<option value="{{ $category->id }}" selected>{{ $category->name }}</option>
						@else
							<option value="{{ $category->id }}">{{ $category->name }}</option>
						@endif
					@endforeach
				</select>

				<!-- BODY -->
				<div id="editor">
					@if( $chatter_editor == 'tinymce' || empty($chatter_editor) )
						<label style="display: none;" id="tinymce_placeholder"></label>
						<textarea id="body" class="richText" name="body" placeholder="Type Your Discussion Here...">{{ old('body') }}</textarea>
					@elseif($chatter_editor == 'simplemde')
						<textarea id="simplemde" name="body" placeholder="Type Your Discussion Here...">{{ old('body') }}</textarea>
					@endif
				</div>

				<input type="hidden" name="_token" id="csrf_token_field" value="{{ csrf_token() }}">

				<p class="uk-text-right">
					<button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
					<button class="uk-button uk-button-primary" id="submit_discussion" type="submit">New Discussion</button>
				</p>
			</form>
		</div>

    </div><!-- #new_discussion -->

</div>

@if( $chatter_editor == 'tinymce' || empty($chatter_editor) )
	<input type="hidden" id="chatter_tinymce_toolbar" value="{{ Config::get('chatter.tinymce.toolbar') }}">
	<input type="hidden" id="chatter_tinymce_plugins" value="{{ Config::get('chatter.tinymce.plugins') }}">
@endif
<input type="hidden" id="current_path" value="{{ Request::path() }}">

@endsection

@section(Config::get('chatter.yields.footer'))


@if( $chatter_editor == 'tinymce' || empty($chatter_editor) )
	<script src="/vendor/devdojo/chatter/assets/vendor/tinymce/tinymce.min.js"></script>
	<script src="/vendor/devdojo/chatter/assets/js/tinymce.js"></script>
	<script>
		var my_tinymce = tinyMCE;
		$('document').ready(function(){
			$('#tinymce_placeholder').click(function(){
				my_tinymce.activeEditor.focus();
			});
		});
	</script>
@elseif($chatter_editor == 'simplemde')
	<script src="/vendor/devdojo/chatter/assets/js/simplemde.min.js"></script>
	<script src="/vendor/devdojo/chatter/assets/js/chatter_simplemde.js"></script>
@endif

<script src="/vendor/devdojo/chatter/assets/vendor/spectrum/spectrum.js"></script>
<script src="/vendor/devdojo/chatter/assets/js/chatter.js"></script>
<script>
	$('document').ready(function(){

		@if (count($errors) > 0)
			$('#new_discussion').hide();
			$('#title').focus();
		@endif


	});
</script>
@stop
