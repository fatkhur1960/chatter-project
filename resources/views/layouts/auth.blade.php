<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('pageTitle') - {{ config('app.name') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fontawesome-all.min.css') }}" rel="stylesheet">
    @yield('css')
    <style type="text/css">
    body, html {
        height: 100%;
        background: url(https://devdojo.com/themes/dd2018/images/auth/bg.png);
        background-size: contain;
        background-position: right center;
        background-repeat: no-repeat;
    }
    .form {
        margin: 80px auto;
        padding: 30px 30px;
        max-width: 300px;
        width: 300px;
    }
    .form h1 {
        font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica, Arial, sans-serif;
        color: #454549;
        font-weight: bold;
        font-size: 20px;
        margin-bottom: 30px;
        text-align: center;
        padding-left: 0;
        text-transform: uppercase;
    }
    .form-group {
        margin-bottom: 15px;
    }
    .form-control {
        border-width: 2px;
        background: #fff;
        color: #424753;
        border: 0;
        border: 2px solid #eaeaea;
        padding-top: 11px;
        padding-bottom: 11px;
        padding-left: 10px;
        box-shadow: 0 0 0;
        height: auto;
        transition: padding .3s ease;
        height: 44px;
        border-radius: 3px;
    }
    .form-control:focus {
        border: 2px solid #19222c;
        border-color: #fc0d1c;
        box-shadow: 0 0 0;
    }
    .btn {
        height: auto;
        width: 100%;
        text-transform: uppercase;
        font-size: 14px;
        border-radius: 3px;
        padding: 14px 20px;
        margin-bottom: 20px;
        display: inline-block;
        font-weight: 400;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        color: #fff;
    }
    .btn-primary, .btn-primary:hover, .btn-primary:focus {
        background-color: #fc0313;
        border-color: #fc0313;
    }
    </style>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
    <div>
        <main>
            @yield('content')
        </main>
    </div>
    @yield('js')
</body>
</html>
