<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @if( Request::is( Config::get('chatter.routes.home')) )
        <title>Title for your forum homepage -  {{ config('app.name') }}</title>
    @elseif( Request::is( Config::get('chatter.routes.home') . '/' . Config::get('chatter.routes.category') . '/*' ) && isset( $discussion ) )
        <title>{{ $discussion->category->name }} - {{ config('app.name') }}</title>
    @elseif( Request::is( Config::get('chatter.routes.home') . '/*' ) && isset($discussion->title))
        <title>{{ $discussion->title }} - {{ config('app.name') }}</title>
    @endif

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/uikit.min.css') }}" />
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet" media="none" onload="this.media='all';">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/1200.css') }}" rel="stylesheet" media="screen and (min-width: 960px) and (max-width: 1199px)">
    <link href="{{ asset('css/960.css') }}" rel="stylesheet" media="screen and (min-width: 640px) and (max-width: 959px)">
    <link href="{{ asset('css/640.css') }}" rel="stylesheet" media="screen and (min-width: 0px) and (max-width: 639px)">

    @yield('css')

    <!-- Scripts -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
    <div id="app">
        <div id="pageLoader" style='display: none;'>
            <div uk-spinner></div>
        </div>
        <div class="uk-nav-container top-nav-container">
            <div class="uk-container-full">
                <nav class="uk-navbar-container uk-navbar-transparent uk-navbar" uk-navbar>
                    <div class="uk-navbar-left">
                        <a class="uk-navbar-item back-to-dashboard uk-button-text " href="#dashboard">My Dashboard</a>
                        <a class="uk-navbar-item back-to-dashboard uk-button-text " href="#browse">Browse</a>
                    </div>

                    <div class="uk-navbar-right">
                        <ul class="uk-navbar-nav">
                        @guest
                            <li class="uk-login">
                                <a href="{{ route('login') }}">{{ __('LOGIN') }}</a>
                            </li>
                            <li class="uk-register">
                                <a href="{{ route('register') }}"><button class="uk-button uk-button-secondary">{{ __('Sign Up') }}</button></a>
                            </li>
                        @else
                            <li>
                                <a class="uk-navbar-toggle uk-search-icon uk-icon" href="#modal-search" uk-search-icon="" uk-toggle="target: #modal-search"></a>
                            </li>
                            <li>
                                <a href="{{ route('create') }}"><i class="dojo-pen"></i></a>
                            </li>
                            <li>
                                <a href="{{ route('messages') }}"><i class="dojo-mail"></i></a>
                            </li>
                            <li class="notifications">
                                <a href="#"><span uk-icon="icon: bell"></span></a>
                                <div class="uk-navbar-dropdown uk-notification-dropdown uk-navbar-dropdown-bottom-right">
                                    <div id="notification-header">
                                        <div id="notification-head-content">
                                            <span uk-icon="icon: bell" class="uk-icon"></span> Notifications
                                        </div>
                                    </div>
                                    <div id="notifications-none" class="">
                                        <span uk-icon="icon: bell; ratio: 2" style="display:block; position:relative; opacity:0.5" class="uk-icon"></span>
                                        All Caught Up!
                                    </div>
                                    <ul class="uk-nav uk-navbar-dropdown-nav">
                                    </ul>
                                    <div id="notification-footer">
                                        <a href="{{ route('notifications') }}"><span uk-icon="icon: eye" class="uk-icon"></span>View All Notifications</a>
                                    </div>
                                </div>
                            </li>
                            <li class="user-info" style="position: relative;">
                                <a href="#" class="user-icon" aria-expanded="false">
                                    <span id="avatar"><img src="https://devdojo.com/media/users/default.png">
                                    <span uk-icon="icon: triangle-down" class="uk-icon"></span>
                                </a>
                                <div class="uk-navbar-dropdown">
                                    <ul class="uk-nav uk-navbar-dropdown-nav">
                                        <li class="user-dropdown-info">
                                            <span id="avatar"><img src="https://devdojo.com/media/users/default.png">
                                            <div>
                                                <p>{{ Auth::user()->name }}</p>
                                                <span>{{ Auth::user()->username }}</span>
                                            </div>
                                        </li>
                                        <li><a href="{{ route('dashboard') }}"><span uk-icon="icon: home" class="uk-icon"></span>&nbsp;&nbsp;My Dashboard</a></li>
                                        <li><a href="{{ route('profile') }}"><span uk-icon="icon: user" class="uk-icon"></span>&nbsp;&nbsp;My Profile</a></li>
                                        <li><a href="{{ route('edit') }}"><span uk-icon="icon: pencil" class="uk-icon"></span>&nbsp;&nbsp;Edit Profile</a></li>
                                        <li><a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();"><span uk-icon="icon: sign-out" class="uk-icon"></span>&nbsp;&nbsp;Logout</a></li>
                                    </ul>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                        </ul>
                    </div>
                </nav>
            </div>
        </div>

        <main class="py-4">
            @yield('content')
        </main>

        <div class="uk-section-default uk-section uk-section-medium footer">
            <div class="uk-container uk-align-center">
                <div class="uk-grid-large uk-grid-margin-large uk-grid" uk-grid="">
                    <div class="uk-width-expand@m uk-width-1-2@s">
                        <div class="uk-margin">
                            <a href="#" class="el-link"><img data-src="https://devdojo.com/themes/dd2018/images/logo.svg" class="el-image" alt="DevDojo" src="https://devdojo.com/themes/dd2018/images/logo.svg" uk-img></a>
                        </div>
                        <div class="uk-margin uk-width-xlarge">Watch. Learn. Create.</div>
                    </div>
                    <div class="uk-width-expand@m uk-width-1-2@s">
                        <ul class="uk-list">
                            <li class="el-item">
                                <div class="el-content">
                                    <a href="#browse" class="el-link uk-link-muted">Browse Our Library</a>
                                </div>
                            </li>
                            <li class="el-item">
                                <div class="el-content">
                                    <a href="#blog" class="el-link uk-link-muted">Tutorials/Articles</a>
                                </div>
                            </li>
                            <li class="el-item">
                                <div class="el-content">
                                    <a href="#scripts/php" class="el-link uk-link-muted">Scripts</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="uk-width-expand@m uk-width-1-2@s">
                        <ul class="uk-list">
                            <li class="el-item">
                                <div class="el-content">
                                    <a href="#points" class="el-link uk-link-muted">Points</a>
                                </div>
                            </li>
                            <li class="el-item">
                                <div class="el-content">
                                    <a href="#help" class="el-link uk-link-muted">Help</a>
                                </div>
                            </li>
                            <li class="el-item">
                                <div class="el-content">
                                    <a href="#page/terms" class="el-link uk-link-muted">Terms of Service</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="uk-width-expand@m uk-width-1-2@s">
                        <div class="uk-margin">
                            <div class="uk-child-width-auto uk-grid-medium uk-grid uk-footer-social" uk-grid="">
                                <div class="uk-first-column">
                                    <a uk-icon="icon: youtube;ratio: 0.8" href="#" target="_blank" class="el-link uk-link-muted uk-icon"></a>
                                </div>
                                <div>
                                    <a uk-icon="icon: instagram;ratio: 0.8" href="#" target="_blank" class="el-link uk-link-muted uk-icon"></a>
                                </div>
                                <div>
                                    <a uk-icon="icon: twitter;ratio: 0.8" href="#" target="_blank" class="el-link uk-link-muted uk-icon"></a>
                                </div>
                                <div>
                                    <a uk-icon="icon: facebook;ratio: 0.8" href="#" target="_blank" class="el-link uk-link-muted uk-icon"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-search" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog uk-flex uk-flex-center uk-flex-middle" uk-height-viewport="" style="box-sizing: border-box; min-height: calc(100vh); height: 318px;">
            <button class="uk-modal-close-full uk-close uk-icon" type="button" uk-close=""></button>
            <form action="/search" method="GET" class="uk-search uk-search-large">
                <input class="uk-search-input uk-text-center" name="value" placeholder="Search..." autofocus="" type="search">
            </form>
        </div>
    </div>

    <script src="{{ asset('js/uikit.min.js') }}"></script>
    <script src="{{ asset('js/uikit-icons.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            if( $(this).width() <= 640 ) {
                $('chatter_body img').find('img').hide();
                console.log($(this).width());
            }
        });
        
        @guest
        document.getElementById('new_discussion_btn').addEventListener('click', function(){
			window.location.href = "{{ url('login') }}";
        });
        @endguest
    </script>
    @yield('js')
</body>
</html>
