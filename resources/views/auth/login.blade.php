@extends('layouts.auth')
@section('pageTitle','Log In')
@section('content')
<div class="form">
    <h1>{{ __('Login to your account') }}</h1>

    <div class="form-body">
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="form-group">
                <input placeholder="{{ __('E-Mail Address') }}" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <input placeholder="{{ __('Password') }}" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group mb-0 justify-content-center">
                <button type="submit" class="btn btn-primary">
                    {{ __('Log in') }}
                </button>

                <div style="text-align: center;">
                    <a class="btn-link" href="{{ route('register') }}">
                        {{ __('or Sign Up?') }}
                    </a>
                    <span style="color:#ddd; padding:0px 10px">/</span>
                    <a class="btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Password?') }}
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
