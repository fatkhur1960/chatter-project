<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/forums');
});

Auth::routes();

Route::get('/home', function() {
    return redirect('/forums');
})->name('home');

Route::get('/create-tutorial', function() {
    return;
})->name('create');

Route::get('/messages', function() {
    return;
})->name('messages');

Route::get('/messages', function() {
    return;
})->name('messages');

Route::get('/notifications', function() {
    return;
})->name('notifications');

Route::get('/dashboard', function() {
    return;
})->name('dashboard');

Route::get('/dashboard/profile', function() {
    return;
})->name('profile');

Route::get('/dashboard/edit', function() {
    return;
})->name('edit');